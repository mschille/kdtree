# kdtree

A minimal KD tree implementation in C++

This code is the k-dimensional equivalent to ```std::sort``` plus
```std::lower_bound``` and ```std::upper_bound```. It allows for efficient
searches of nearest neighbour (of n nearest neighbours) of a given point
in k-dimensional space. It is similar to ```std::sort``` in that it needs
a comparison function. It will also need a function to measure distances
between points.

A simple example of the code in use is given below:

```c++
using point = std::array<float, 2>; // 2D points
const auto cmp = [] (const point& a, const point& b,
    const auto& dim) noexcept { return a[dim] < b[dim]; };
const auto dist = [] (const point& a, const point& b,
    const auto& dim) noexcept {
	if (std::size_t(-1) == dim) { // full distance
	    // a point is not it's own nearest neighbour (depends
	    // on application if you want this...)
	    if (&a == &b) return std::numeric_limits<float>::max();
	    // full distance between points - we use squared distance
	    // here to save a square root
	    return (a[0] - b[0]) * (a[0] - b[0]) +
		(a[1] - b[1]) * (a[1] - b[1]);
	} else { // distance in coordinate dim only
	    return (a[dim] - b[dim]) * (a[dim] - b[dim]);
	}
    };

// okay, get some points from somewhere:
using Points = std::vector<point>;
Points v = /* from somewhere... */;
// build kd tree
build_kdtree<2>(v.begin(), v.end(), cmp);
// find nearest neighbour to a point
const auto& p = *(v.begin() + 42); // some element - need not be one from v
auto nearest = find_nearest_kdtree<2>(v.begin(), v.end(), p, cmp, dist);
std::cout << "nearest is (" << (*nearest)[0] << "," << (*nearest)[1] << ")"
    << std::endl;

// you can also find the n nearest neighbours:

// pair of iterator (to neighbour), and its distance to a point
using NeighbourWithDistance = std::pair<Points::iterator, float>;
// array of five of these
using FiveBest = std::array<NeighbourWithDistance, 5>;

// prepare an array, fill with "nothing found"
FiveBest best;
best.fill(std::make_pair(v.end(), std::numeric_limits<float>::max()));
// find the five nearest neighbours to p
find_n_nearest_kdtree<2>(v.begin(), v.end(), p, best.begin(), best.end(),
    cmp, dist);
for (const auto& n: best) {
    std::cout << "near neighbour is (" << (*n.first)[0] << ","
	<< (*n.first)[1] << ") dist" << n.second << std::endl;
}
```
