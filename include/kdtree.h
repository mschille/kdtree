/** @file kdtree.h
 *
 * @brief KD tree
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2016-09-01
 *
 * For copyright and license information, see the end of the file.
 */

#ifndef KDTREE_H
#define KDTREE_H

#include <utility>
#include <algorithm>
#include <stdexcept>
#include <type_traits>

// namespace to contain the various bits needed to operate on KD trees
namespace kdtree_impl {
    /// default KD tree comparison semantics
    struct default_compare {
        template <typename S, typename T, typename COORD>
        bool operator()(const S& a, const T& b, const COORD& coord) const
                noexcept(noexcept(a[std::size_t(coord)] < b[std::size_t(coord)]))
        { return a[std::size_t(coord)] < b[std::size_t(coord)]; }
    };
    /// default KD tree distance measurement
    template <std::size_t NDIM>
    struct default_distance {
        template <typename S, typename T, typename COORD>
        auto operator()(const S& a, const T& b, const COORD& coord) const
                noexcept(noexcept(a[std::size_t(coord)] -
                                  b[std::size_t(coord)]))
        {
            // be on the safe side
            if (std::size_t(-1) != std::size_t(coord)) {
                const auto d = a[std::size_t(coord)] - b[std::size_t(coord)];
                return d * d;
            } else {
                decltype(a[0] - b[0]) retVal = 0;
                for (std::size_t i = 0; NDIM != i; ++i) {
                    const auto d = a[i] - b[i];
                    retVal += d * d;
                }
                return retVal;
            }
        }
    };

    /// an object that does nothing
    struct default_nop {
        template <typename... ARGS>
        void operator()(ARGS&&... /* unused */) const noexcept {}
    };

    /// return comparison object bound to compare a specific coordinate
    template <std::size_t COORD, typename CMP>
    struct _cmp_coord{
        const CMP& cmp;
        template <typename S, typename T>
        bool operator()(const S& a, const T& b) const noexcept(noexcept(
                cmp(a, b, std::integral_constant<std::size_t, COORD>())))
        {
            return cmp(a, b, std::integral_constant<std::size_t, COORD>());
        }
    };
    template <std::size_t COORD, typename CMP>
    _cmp_coord<COORD, CMP> cmp_coord(const CMP& cmp) noexcept
    { return {cmp}; }

    template <std::size_t BIND, std::size_t COORD, typename CMP, typename T>
    struct _bind_cmp_coord {
        static_assert(1 == BIND || 2 == BIND, "BIND must be 1 or 2");
        const CMP& cmp;
        const T& arg;
        template <typename A>
        bool operator()(const A& a) const noexcept(noexcept(
                cmp(arg, a, std::integral_constant<std::size_t, COORD>())))
        {
            constexpr std::integral_constant<std::size_t, COORD> coord{};
            return 1 == BIND ? cmp(arg, a, coord) : cmp(a, arg, coord);
        }
    };
    template <std::size_t BIND, std::size_t COORD, typename CMP, typename T>
    _bind_cmp_coord<BIND, COORD, CMP, T> bind_cmp_coord(const CMP& cmp,
                                                        const T& arg) noexcept
    { return {cmp, arg}; }

    /// do all the hard work of constructing a KD tree
    template <std::size_t NDIM, std::size_t COORD, typename IT, typename CMP>
    void _build_kdtree(IT begin, IT end, CMP cmp) noexcept(noexcept(
            std::nth_element(begin, begin, end, cmp_coord<COORD>(cmp))))
    {
        static_assert(NDIM > 0, "number of dimensions must be positive");
        static_assert(COORD < NDIM, "coordinate numbers run from 0 to NDIM - 1");
        // termination condition (we're done if there's at most element between
        // begin and end)
        const auto dist = end - begin;
        if (1 >= dist) return;
        // work out middle, and partition
        const auto mid = begin + dist / 2;
        std::nth_element(begin, mid, end, cmp_coord<COORD>(cmp));
        // build KD tree in both subranges
        _build_kdtree<NDIM, (COORD + 1) % NDIM, IT, CMP>(begin, mid, cmp);
        _build_kdtree<NDIM, (COORD + 1) % NDIM, IT, CMP>(mid + 1, end, cmp);
    }

    /// all the hard work needed to check if a sequence is a KD tree structure
    template <std::size_t NDIM, std::size_t COORD, typename IT, typename CMP>
    bool _is_kdtree(IT begin, IT end, CMP cmp) noexcept(noexcept(
            std::find_if(begin, begin, bind_cmp_coord<1, COORD>(cmp, *begin))))
    {
        static_assert(NDIM > 0, "number of dimensions must be positive");
        static_assert(COORD < NDIM, "coordinate numbers run from 0 to NDIM - 1");
        // termination condition (we're done if there's at most element between
        // begin and end)
        const auto dist = end - begin;
        if (1 >= dist) return true;
        // work out middle, and check partitioning property
        const auto mid = begin + dist / 2;
        if (mid != std::find_if(begin, mid,
                                bind_cmp_coord<1, COORD>(cmp, *mid)) ||
            end != std::find_if(mid + 1, end,
                                bind_cmp_coord<2, COORD>(cmp, *mid)))
            return false;
        // check KD tree in both subranges
        return _is_kdtree<NDIM, (COORD + 1) % NDIM, IT, CMP>(begin, mid, cmp) &&
            _is_kdtree<NDIM, (COORD + 1) % NDIM, IT, CMP>(mid + 1, end, cmp);
    }

    /// little helper that finds nearest neighbour(s) in a kd tree
    template <std::size_t NDIM, typename VAL, typename IT, typename DISTTYPE,
              typename CMP, typename DIST, typename UPDATE>
    struct nearest_finder {
        static_assert(NDIM > 0, "number of dimensions must be positive");
        const VAL& val;
        const CMP& cmp;
        const DIST& dist;
        std::pair<IT, DISTTYPE>& best;
        const UPDATE& update;

        /// general kd tree find - implementation
        template <std::size_t COORD = 0>
        void operator()(IT begin, IT end) const noexcept(
                noexcept(cmp(*begin, *begin,
                             std::integral_constant<std::size_t, COORD>())) &&
                noexcept(dist(*begin, *begin,
                              std::integral_constant<std::size_t, std::size_t(-1)>())) &&
                noexcept(update()))
        {
            static_assert(COORD < NDIM,
                          "coordinate numbers run from 0 to NDIM - 1");
            constexpr std::integral_constant<std::size_t, COORD> coord{};
            constexpr std::integral_constant<std::size_t, std::size_t(-1)>
                    allcoords{};
            // get partitioning point and build the two subtrees
            const IT mid = begin + (end - begin) / 2;
            // decide which subtree to check first - try to avoid branching
            // (since stack space tends to be cheap)
            const IT _trees[6] = {begin, mid, mid + 1, end, begin, mid};
            const auto* trees = &_trees[2 * bool(cmp(*mid, val, coord))];
            // look in first subtree
            if (trees[1] != trees[0]) {
                operator()<(COORD + 1) % NDIM>(trees[0], trees[1]);
            }
            // see if we can prove that all elements in the other subtree are
            // further away
            if (best.second < dist(*mid, val, coord)) return;
            // check mid itself
            const auto mydist = dist(*mid, val, allcoords);
            if (mydist < best.second) {
                best = std::make_pair(mid, mydist);
                update();
            }
            // look in other subtree
            if (trees[3] != trees[2]) {
                operator()<(COORD + 1) % NDIM>(trees[2], trees[3]);
            }
        }
    };

    template <std::size_t NDIM, typename VAL, typename IT, typename DISTTYPE,
              typename CMP, typename DIST, typename UPDATE>
    nearest_finder<NDIM, VAL, IT, DISTTYPE, CMP, DIST, UPDATE>
    make_nearest_finder(
            const VAL& val, std::pair<IT, DISTTYPE>& best, const CMP& cmp,
            const DIST& dist,
            const UPDATE&
                    update) noexcept(noexcept(nearest_finder<NDIM, VAL, IT,
                                                             DISTTYPE, CMP,
                                                             DIST, UPDATE>{
            val, cmp, dist, best, update}))
    {
        return {val, cmp, dist, best, update};
    }
}

/** @brief build a KD tree
 *
 * A KD tree is a k-dimensional search tree that allows fast search in one
 * or more dimensions. This works in close analogy to fast searches in one
 * dimension which can be implemented by sorting the objects among which an
 * element is to be found, and then traversing the binary search tree that
 * is implicit in the a binary search algorithm. For a one-dimensional
 * problem, building a KD tree is equivalent to sorting. In the general
 * case, the algorithm works by partitioning the array in a very similar
 * manner to quicksort, but using the various coordinates/dimensions
 * cyclically for subsequent partitioning operations. The order of the
 * elements in the given range is permuted to form the KD tree.
 *
 * @tparam NDIM         dimensionality of the problem
 * @tparam IT           random access iterator
 * @tparam CMP          type of comparison funtion
 *
 * @param begin         begin of range to be turned into a KD tree
 * @param end           end of range to be turned into KD tree
 * @param cmp           comparison function to be used
 *
 * The comparison function cmp compares two elements in the range pointed to
 * by begin and end. It takes an additional third parameter which converts to
 * std::size_t, specifying in which dimension (0 ... (NDIM - 1)) the two
 * elements are to be compared. The comparison function typically gets called
 * with different types for the third parameter, so lambdas should take care
 * of making the third parameter auto, and functors should template the
 * third parameter to make sure the compiler can stamp out specialised
 * versions of that function. (See below for an example.)
 *
 * To build a KD tree from a vector of 4-dimensional points, one could use
 * this code:
 * @code
 * typedef std::array<float, 4> Point;
 * std::vector<Point> v = fill_v_somehow();
 * build_kdtree<4>(v.begin(), v.end());
 * // v contains a permutation of the original elements that forms a KD tree
 * @endcode
 * The default comparison function works for anything in the range pointed
 * to by [begin, end[ that offers indexing (either natively, or with
 * operator[]). To illustrate the use of a comparison function in such a
 * more complicated case, see this example:
 * @code
 * class Point { // a point (x, y)
 * private:
 *     float m_x, m_y; // coordinates
 * public:
 *     // ...
 *     float x() const noexcept { return m_x; }
 *     float y() const noexcept { return m_y; }
 * };
 * std::vector<Point> v = fill_v_somehow();
 * build_kdtree<2>(v.begin(), v.end(),
 *     [] (const Point& a, const Point& b, const auto dim) noexcept
 *     {   // dim is a std::integral_constant<std::size_t, DIM>,
 *         // and it'll convert down to a compile-time std::size_t
 *         switch (dim) { // which dimension to look at
 *         case 0: return a.x() < b.x();
 *         case 1: return a.y() < b.y();
 *         default: assert(false);
 *         };
 *     });
 * // v contains a permutation of the original elements that forms a KD tree
 * @endcode
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2016-09-01
 */
template <std::size_t NDIM, typename IT,
          typename CMP = kdtree_impl::default_compare>
void build_kdtree(
        IT begin, IT end,
        CMP cmp = kdtree_impl::default_compare()) noexcept(noexcept(
            kdtree_impl::_build_kdtree<NDIM, 0, IT, CMP>(begin, end, cmp)))
{ kdtree_impl::_build_kdtree<NDIM, 0, IT, CMP>(begin, end, cmp); }

/** @brief check if a range of elements forms a KD tree
 *
 * This routine checks if the range given by iterators begin and end is a KD
 * tree according to the given comparison function.
 *
 * @tparam NDIM         dimensionality of the problem
 * @tparam IT           random access iterator
 * @tparam CMP          type of comparison funtion
 *
 * @param begin         begin of range to be checked if it is a KD tree
 * @param end           end of range to be checked if it is a KD tree
 * @param cmp           comparison function to be used
 *
 * @returns             true if (between, end( is a KD tree
 *
 * See build_kdtree for details on the compasion function, and on how to
 * construct a KD tree.
 *
 * To check if a sequence of elements is a KD tree from a vector of
 * 4-dimensional points, one could use this code:
 * @code
 * typedef std::array<float, 4> Point;
 * std::vector<Point> v = fill_v_somehow();
 * build_kdtree<4>(v.begin(), v.end());
 * // v contains a permutation of the original elements that forms a KD tree
 *
 * // do something with w that may destroy the KD tree property, e.g:
 * v.erase(std::remove_if(v.begin(), v.end(), [] (const Point& p)
 *     {return point[0] < 0.; } , v.end());
 *
 * // and restore the KD tree property if required
 * if (check_kdtree<4>(v.begin(), v.end()))
 *     build_kdtree<4>(v.begin(), v.end());
 * @endcode
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2016-09-01
 */
template <std::size_t NDIM, typename IT,
         typename CMP = kdtree_impl::default_compare>
bool is_kdtree(IT begin, IT end,
        CMP cmp = kdtree_impl::default_compare()) noexcept(
        noexcept(kdtree_impl::_is_kdtree<NDIM, 0, IT, CMP>(begin, end, cmp)))
{ return kdtree_impl::_is_kdtree<NDIM, 0, IT, CMP>(begin, end, cmp); }

/** @brief find the element nearest to given value in a KD tree
 *
 * This routine finds the element nearest to a given value in the range
 * given by iterators begin and end.
 *
 * @tparam NDIM         dimensionality of the problem
 * @tparam IT           random access iterator
 * @tparam T            type of the value to search for
 * @tparam CMP          type of comparison funtion
 * @tparam DIST         type of distance metric
 * @tparam RTYPE        type pointed to by IT
 * @tparam DISTTYPE     type returned by distance measure
 *
 * @param begin         begin of KD tree range
 * @param end           end of KD tree range
 * @param value         value to search for (or nearst neighbour)
 * @param cmp           comparison function to be used (default, if omitted)
 * @param dist          distance metric (or default, if parameter is omitted)
 * @param maxdist       maximum distance accepted - if measured distance
 *                      between points is larger than this, the result is
 *                      ignored (default is
 *                      std::numeric_limits<DISTTYPE>::max())
 *
 * @returns             a pair of an iterator and its distance to value
 *
 * Here's and example of how to use the routine:
 * @code
 * typedef std::array<float, 4> Point;
 * std::vector<Point> v = fill_v_somehow();
 * build_kdtree<4>(v.begin(), v.end());
 * // v contains a permutation of the original elements that forms a KD tree
 *
 * // distance measure that will exclude the searched-for point
 * const auto dist = [] (const Point& a, const Point& b, const auto dim) {
 *     // dim is an instance of std::integral_constant<std::size_t, N> which
 *     // implicitly converts to size_t
 *     if (std::size_t(-1) == dim) {
 *         // normal distance measure - all dimensions
 *         // exclude the searched-for point itself
 *         if (&a == &b || a == b) return std::numeric_limits<float>::max();
 *         float sum = 0;
 *         for (unsigned i = 0; i < 4; ++i)
 *             sum += (a[i] - b[i]) * (a[i] - b[i]);
 *         return sum;
 *     } else {
 *         // only distance along dimension dim
 *         return (a[dim] - b[dim]) * (a[dim] - b[dim]);
 *     }
 * };
 *
 * // find points closest to v[2]
 * auto best = find_nearest_kdtree<4>(v.begin(), v.end(), v[2],
 *     [] (const Point& a, const Point& b, int dim)
 *     { return a[dim] < b[dim]; }, dist);
 * // best is now a pair of iterator, best distance
 * assert(dist(v[2], *best.first, -1) == best.second);
 * std::printf("nearest neighbour (%f, %f, %f, %f)\n",
 *     (*best.first)[0], (*best.first)[1], (*best.first)[2],
 *     (*best.first)[3]);
 * @endcode
 *
 * See build_kdtree for details on the compasion function, and on how to
 * construct a KD tree. The distance function has this signature:
 *
 * @code
 * DISTTYPE dist(const T& a, const T& b, const auto coord);
 * @endcode
 *
 * It measures the distance between points a and b. Conceptually, the
 * parameter coord can be -1, in which case the full distance is evaluated,
 * or it can be a positive number smaller than NDIM, in which case only the
 * distance along the dimension specified by coord is evaluated. In
 * practise, the type of coord changes for different call sites, and you
 * should expect types std::integral_constant<std::size_t, COORD>. (This is
 * done to help the compiler inline specialised version of the distance
 * metric.)
 *
 * A valid distance measure satisfies these properties:
 *
 * 1. dist(a, b) >= 0 for any points a, b
 * 2. dist(a, b) == dist(b, a) for any points a, b
 * 3. dist(a, c) <= dist(a, b) + dist(b, c) for any points a, b, c
 *
 * These relations must hold for any valid fixed value for the third
 * argument. In addition to that, one has to obey the following relations:
 *
 * 4. dist(a, b, -1) >= dist(a, b, k) for k >= 0
 * 5. dist(a, a, k) == 0 for k >= 0
 * 6. dist(a, a, -1) is either 0 or std::numeric_limits<DISTTYPE>::max()
 *
 * Depending on which alternative is chosen in 6., find_nearest_kdtree will
 * either include the searched-for point in its search, or exclude it from
 * the search by returning std::numeric_limits<DISTTYPE>::max() -- this way
 * one can search for the nearest point that is not the searched-for value
 * itself. Valid examples of a distance function would be the Euclidian
 * distance, or the sum of absolute values of coordinate differences over
 * all dimensions, or the maximum of the absolute value of coordinate
 * differences of all dimensions. Note that the rules above allow you skip
 * the final square root that would be needed in the calculation of the
 * Euclidian distance to save CPU cycles, and you'd still get the same result.
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2016-09-01
 */
template <std::size_t NDIM, typename IT, typename T,
          typename CMP = kdtree_impl::default_compare,
          typename DIST = kdtree_impl::default_distance<NDIM>,
          typename RTYPE =
                  typename std::decay<decltype(*std::declval<IT>())>::type,
          typename DISTTYPE = decltype(std::declval<const DIST&>()(
                  std::declval<const RTYPE&>(), std::declval<const RTYPE&>(),
                  std::integral_constant<std::size_t, 0>()))>
std::pair<IT, DISTTYPE> find_nearest_kdtree(
        IT begin, IT end, const T& value,
        CMP cmp = kdtree_impl::default_compare(),
        DIST dist = kdtree_impl::default_distance<NDIM>(),
        DISTTYPE maxdist = std::numeric_limits<DISTTYPE>::
                max()) noexcept(noexcept(kdtree_impl::
                                                 make_nearest_finder<NDIM>(
                                                         value,
                                                         std::declval<std::pair<
                                                                 IT,
                                                                 DISTTYPE>&>(),
                                                         cmp, dist,
                                                         kdtree_impl::
                                                                 default_nop())(
                                                         begin, end)))
{
    std::pair<IT, DISTTYPE> best(end, maxdist);
    if (end != begin) {
        kdtree_impl::make_nearest_finder<NDIM>(value, best, cmp, dist,
                                         kdtree_impl::default_nop())(begin,
                                                                     end);
    }
    return best;
}

/** @brief find the element nearest to given value in a KD tree
 *
 * This routine finds the element nearest to a given value in the range
 * given by iterators begin and end.
 *
 * @tparam NDIM         dimensionality of the problem
 * @tparam IT           random access iterator
 * @tparam T            type of the value to search for
 * @tparam JT           type of iterator into (IT, distance pairs)
 * @tparam CMP          type of comparison funtion
 * @tparam DIST         type of distance metric
 * @tparam RTYPE        type pointed to by IT
 * @tparam DISTTYPE     type returned by distance measure
 *
 * @param begin         begin of KD tree range
 * @param end           end of KD tree range
 * @param value         value to search for (or nearst neighbour)
 * @param first         begin of range in which to save pairs (IT, distance)
 * @param last          end of range in which to save pairs (IT, distance)
 * @param cmp           comparison function to be used (default, if omitted)
 * @param dist          distance metric (or default, if parameter is omitted)
 * @param maxdist       maximum distance accepted - if measured distance
 *                      between points is larger than this, the result is
 *                      ignored (default is
 *                      std::numeric_limits<DISTTYPE>::max())
 *
 * Here's and example of how to use the routine:
 * @code
 * typedef std::array<float, 4> Point;
 * std::vector<Point> v = fill_v_somehow();
 * build_kdtree<4>(v.begin(), v.end());
 * // v contains a permutation of the original elements that forms a KD tree
 *
 * // distance measure that will exclude the searched-for point
 * const auto dist = [] (const Point& a, const Point& b, const auto dim) {
 *     switch (dim) {
 *     case -1: // normal distance measure - all dimensions
 *         // exclude the searched-for point itself
 *         {
 *             float sum = 0;
 *             for (unsigned i = 0; i < 4; ++i)
 *                 sum += (a[i] - b[i]) * (a[i] - b[i]);
 *             return sum;
 *         }
 *     default: // only distance along dimension dim
 *         return (a[dim] - b[dim]) * (a[dim] - b[dim]);
 *     }
 * };
 *
 * // find 5 closest points closest to v[2]
 * std::array<std::pair<std::vector<Point>::iterator, float>, 5> best;
 * best.fill(std::make_pair(v.end(), std::numeric_limits<float>::max()));
 * find_n_nearest_kdtree<4>(v.begin(), v.end(), v[2], best.begin(),
 *     best.end(), [] (const Point& a, const Point& b, const auto dim)
 *     noexcept { return a[dim] < b[dim]; }, dist);
 * // best now contains 5 pairs of (iterator into v, distance) in order of
 * // ascending distance
 * @endcode
 *
 * See build_kdtree for details on the compasion function, and on how to
 * construct a KD tree. The distance function is explained in more detail in
 * the documentation for find_nearest_kdtree.
 *
 * You can pre-populate the range (first, last( if you have suitable
 * candidates to do so, provided you keep the range sorted by increasing
 * distance. This routine maintains this invariant.
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2016-09-01
 */
template <std::size_t NDIM, typename IT, typename T, typename JT,
          typename CMP = kdtree_impl::default_compare,
          typename DIST = kdtree_impl::default_distance<NDIM>,
          typename RTYPE =
                  typename std::decay<decltype(*std::declval<IT>())>::type,
          typename DISTTYPE = decltype(std::declval<const DIST&>()(
                  std::declval<const RTYPE&>(), std::declval<const RTYPE&>(),
                  std::integral_constant<std::size_t, 0>()))>
void find_n_nearest_kdtree(IT begin, IT end, const T& value,
        JT first, JT last, CMP cmp = kdtree_impl::default_compare(),
        DIST dist = kdtree_impl::default_distance<NDIM>()) noexcept(
#ifdef NDEBUG
        true
#else
        false
#endif
        && noexcept(kdtree_impl::make_nearest_finder<NDIM>(
                   value, std::declval<std::pair<IT, DISTTYPE>&>(), cmp, dist,
                   kdtree_impl::default_nop())(begin, end)))
{
    static_assert(std::is_same<decltype(*first),
            std::pair<IT, DISTTYPE>&>::value,
            "Range of best candidates is of wrong type!");
#ifndef NDEBUG
    if (last == first)
        throw std::length_error("destination array between first and last "
                "must have non-zero size in find_n_nearest_kdtree");
#endif
    using GoodPoint = std::pair<IT, DISTTYPE>;
    if (end != begin) {
        const auto nneigh = std::distance(first, last);
        if (1 == nneigh) {
            kdtree_impl::make_nearest_finder<NDIM>(
                    value, *(last - 1), cmp, dist,
                    kdtree_impl::default_nop())(begin, end);
        } else if (nneigh < 16) { // [FIXME] to be tuned
            // for moderate sizes, we use a simple updating strategy
            const auto update = [ first, last ]() noexcept
            {
                std::rotate(std::upper_bound(first, last - 1, *(last - 1),
                                             [](const GoodPoint& a,
                                                const GoodPoint& b) noexcept {
                                                 return a.second < b.second;
                                             }),
                            last - 1, last);
            };
            // find the nearest points
            kdtree_impl::make_nearest_finder<NDIM>(value, *(last - 1), cmp,
                                                   dist, update)(begin, end);
        } else {
            // we need many neighbours, go for the most complex thing:
            // we maintain a heap containing the best points found so far. for
            // that, we need a comparison function for the heap, and an update
            // procedure that _find_nearest_kdtree below calls when it's
            // updated the worst hit so far...
            const auto heapcmp =
                    [](const GoodPoint& a, const GoodPoint& b) noexcept
            {
                return a.second < b.second;
            };
            const auto update = [ first, last, &heapcmp ]() noexcept
            {
                // after restoring the heap to include the newly added
                // (better) point...
                std::push_heap(first, last, heapcmp);
                // ... put the worst point so far into the last position
                std::pop_heap(first, last, heapcmp);
            };
            // build a heap from the previous "best" points
            std::make_heap(first, last, heapcmp);
            // put the one with the largest distance to value in the last
            // position
            std::pop_heap(first, last, heapcmp);
            // find the nearest points
            kdtree_impl::make_nearest_finder<NDIM>(value, *(last - 1), cmp,
                                                   dist, update)(begin, end);
            // restore the heap
            std::push_heap(first, last, heapcmp);
            // and sort in order of ascending distance...
            std::sort_heap(first, last, heapcmp);
        }
    }
}
#endif // KDTREE_H

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
