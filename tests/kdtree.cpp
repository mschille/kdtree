/** @file kdtree.cpp
 *
 * @brief KD tree unit tests
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2016-09-01
 *
 * For copyright and license information, see the end of the file.
 */

#include <array>
#include <ctime>
#include <cstdio>
#include <cassert>
#include <limits>
#include <random>
#include <numeric>
#include <utility>
#include <functional>
#include <type_traits>

#include "kdtree.h"

#ifndef __GNUC__
#define report(str, ...) std::printf("In %s: " str, __func__, __VA_ARGS__)
#else
#define report(str, ...) std::printf("In %s: " str, __PRETTY_FUNCTION__, __VA_ARGS__)
#endif


template <typename T = float>
std::pair<unsigned, unsigned> test1D(
        unsigned ntimes = 1 << 7, unsigned maxelem = 1 << 8)
{
    // a 1D KD tree will just produce sorted output, so we can check with
    // std::is_sorted
    static_assert(std::is_integral<T>::value ||
            std::is_floating_point<T>::value,
            "T must be either integer or floating point type.");
    unsigned ntot = 0, nfail = 0;
    std::vector<std::array<T, 1> > v;
    v.reserve(maxelem);
    std::mt19937 rng(unsigned(std::time(nullptr)));
    typename std::conditional<std::is_integral<T>::value,
             std::uniform_int_distribution<T>,
             std::uniform_real_distribution<T> >::type u(
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::min() : T(0),
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::max() : T(1));

    for (unsigned i = 0; i < maxelem; ++i) {
        for (unsigned j = 0; j < ntimes; ++j, ++ntot) {
            // fill v with random numbers, length i
            v.clear();
            for (unsigned k = 0; k < i; ++k)
                v.push_back({{ u(rng) }});
            // build a 1D tree
            build_kdtree<1>(v.begin(), v.end());
            // and check if the tree is built correctly
            if (!std::is_sorted(v.begin(), v.end())) ++nfail;
        }
    }
    report("Summary: %u/%u tests failed\n", nfail, ntot);
    return std::make_pair(nfail, ntot);
}

template <unsigned K, typename T = float>
std::pair<unsigned, unsigned> testKD(
        unsigned ntimes = 1 << 7, unsigned maxelem = 1 << 8)
{
    // build a k-dimensional kd tree, and check that its invariant holds
    // (is_kdtree)
    static_assert(std::is_integral<T>::value ||
            std::is_floating_point<T>::value,
            "T must be either integer or floating point type.");
    static_assert(K > 0, "dimension K must be positive.");
    unsigned ntot = 0, nfail = 0;
    std::vector<std::array<T, K> > v;
    v.reserve(maxelem);
    std::mt19937 rng(unsigned(std::time(nullptr)));
    typename std::conditional<std::is_integral<T>::value,
             std::uniform_int_distribution<T>,
             std::uniform_real_distribution<T> >::type u(
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::min() : T(0),
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::max() : T(1));

    for (unsigned i = 0; i < maxelem; ++i) {
        for (unsigned j = 0; j < ntimes; ++j, ++ntot) {
            // fill v with random numbers, length i
            v.clear();
            for (unsigned k = 0; k < i; ++k) {
                v.emplace_back(std::array<T, K>());
                for (unsigned l = 0; l < K; ++l)
                    v.back()[l] = u(rng);
            }
            // build a KD tree
            build_kdtree<K>(v.begin(), v.end());
            // and check if the tree is built correctly
            if (!is_kdtree<K>(v.begin(), v.end())) ++nfail;
        }
    }
    report("Summary: %u/%u tests failed\n", nfail, ntot);
    return std::make_pair(nfail, ntot);
}

template <unsigned K, typename T = float>
std::pair<unsigned, unsigned> testNN1(
        unsigned ntimes = 1 << 7, unsigned maxelem = 1 << 8)
{
    // build a kdtree, and for each point in it, find the nearest point
    // (should be the same point, or one with distance zero, in case of
    // multiple identical points in the tree)
    static_assert(std::is_integral<T>::value ||
            std::is_floating_point<T>::value,
            "T must be either integer or floating point type.");
    static_assert(K > 0, "dimension K must be positive.");
    unsigned ntot = 0, nfail = 0;
    std::vector<std::array<T, K> > v;
    v.reserve(maxelem);
    std::mt19937 rng(unsigned(std::time(nullptr)));
    typename std::conditional<std::is_integral<T>::value,
             std::uniform_int_distribution<T>,
             std::uniform_real_distribution<T> >::type u(
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::min() : T(0),
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::max() : T(1));

    for (unsigned i = 0; i < maxelem; ++i) {
        for (unsigned j = 0; j < ntimes; ++j, ++ntot) {
            // fill v with random numbers, length i
            v.clear();
            for (unsigned k = 0; k < i; ++k) {
                v.emplace_back(std::array<T, K>());
                for (unsigned l = 0; l < K; ++l)
                    v.back()[l] = u(rng);
            }
            // build a KD tree
            build_kdtree<K>(v.begin(), v.end());
            // look for the nearest point in the kdtree for each point in it
            // (should yield the point itself)
            bool okay = true;
            for (auto it = v.begin(), end = v.end(); end != it; ++it) {
                auto needle = find_nearest_kdtree<K>(v.begin(), end, *it);
                if (needle.first != it && 0 != needle.second) {
                    okay = false;
                    break;
                }
                assert(T(0) == needle.second);
            }
            if (!okay) ++nfail;
        }
    }
    report("Summary: %u/%u tests failed\n", nfail, ntot);
    return std::make_pair(nfail, ntot);
}

namespace {
    template <std::size_t K>
    struct _cmp {
        template <typename T, typename DIM>
        bool operator()(const std::array<T, K>& a, const std::array<T, K>& b,
                        const DIM& dim) const noexcept
        {
            return a[dim] < b[dim];
        }
    };
    template <std::size_t K, bool SELFATINFINITY>
    struct _dist {
        template <typename T, typename DIM>
        T operator()(const std::array<T, K>& a, const std::array<T, K>& b,
                     const DIM& dim) const noexcept
        {
            if (std::size_t(-1) == dim) {
                if (SELFATINFINITY) {
                    if (&a == &b) return std::numeric_limits<T>::max();
                }
                T sum = 0;
                for (std::size_t i = 0; K != i; ++i) {
                    const auto d = a[i] - b[i];
                    sum += d * d;
                }
                return sum;
            } else {
                const auto d = a[dim] - b[dim];
                return d * d;
            }
        }
    };
}
template <unsigned K, typename T = float>
std::pair<unsigned, unsigned> testNN2(
        unsigned ntimes = 1 << 7, unsigned maxelem = 1 << 8)
{
    // build a kdtree, and for each point in it, find the nearest point
    // (excluding the point itself)
    static_assert(std::is_integral<T>::value ||
            std::is_floating_point<T>::value,
            "T must be either integer or floating point type.");
    static_assert(K > 0, "dimension K must be positive.");
    unsigned ntot = 0, nfail = 0;
    std::vector<std::array<T, K> > v;
    v.reserve(maxelem);
    std::mt19937 rng(unsigned(std::time(nullptr)));
    typename std::conditional<std::is_integral<T>::value,
             std::uniform_int_distribution<T>,
             std::uniform_real_distribution<T> >::type u(
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::min() : T(0),
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::max() : T(1));

    for (unsigned i = 0; i < maxelem; ++i) {
        for (unsigned j = 0; j < ntimes; ++j, ++ntot) {
            // fill v with random numbers, length i
            v.clear();
            for (unsigned k = 0; k < i; ++k) {
                v.emplace_back(std::array<T, K>());
                for (unsigned l = 0; l < K; ++l)
                    v.back()[l] = u(rng);
            }
            // build a KD tree
            build_kdtree<K>(v.begin(), v.end());
            // define kdtree comparison function, and distance metric
            _cmp<K> cmp;
            _dist<K, true> dist;
            // look for the nearest point in the kdtree for each point in it
            // (should yield the point itself)
            bool okay = true;
            for (auto it = v.begin(), end = v.end(); end != it; ++it) {
                auto needle = find_nearest_kdtree<K>(v.begin(), end, *it, cmp, dist);
                // must not find a point closer than needle
                auto better = std::find_if(v.begin(), v.end(),
                        [needle, it, &dist] (const std::array<T, K>& el) {
                        return (&(*it) != &el) &&
                            dist(*it, el, std::size_t(-1)) < needle.second; });
                if (v.end() != better) {
                    okay = false;
                    break;
                }
            }
            if (!okay) ++nfail;
        }
    }
    report("Summary: %u/%u tests failed\n", nfail, ntot);
    return std::make_pair(nfail, ntot);
}

template <unsigned K, typename T = float>
std::pair<unsigned, unsigned> test5NN(
        unsigned ntimes = 1 << 7, unsigned maxelem = 1 << 8)
{
    // build a kdtree, and for each point in it, find the nearest point
    // (excluding the point itself)
    static_assert(std::is_integral<T>::value ||
            std::is_floating_point<T>::value,
            "T must be either integer or floating point type.");
    static_assert(K > 0, "dimension K must be positive.");
    unsigned ntot = 0, nfail = 0;
    std::vector<std::array<T, K> > v;
    typedef std::pair<typename std::vector<std::array<T, K> >::iterator, T> GoodPoint;
    typedef std::array<GoodPoint, 5> FiveBestPoints;
    v.reserve(maxelem);
    std::mt19937 rng(unsigned(std::time(nullptr)));
    typename std::conditional<std::is_integral<T>::value,
             std::uniform_int_distribution<T>,
             std::uniform_real_distribution<T> >::type u(
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::min() : T(0),
                     std::is_integral<T>::value ?
                     std::numeric_limits<T>::max() : T(1));

    for (unsigned i = 0; i < maxelem; ++i) {
        for (unsigned j = 0; j < ntimes; ++j, ++ntot) {
            // fill v with random numbers, length i
            v.clear();
            for (unsigned k = 0; k < i; ++k) {
                v.emplace_back(std::array<T, K>());
                for (unsigned l = 0; l < K; ++l)
                    v.back()[l] = u(rng);
            }
            // build a KD tree
            build_kdtree<K>(v.begin(), v.end());
            // look for the nearest five points to each point in v
            bool okay = true;
            // define kdtree comparison function, and distance metric
            _cmp<K> cmp;
            _dist<K, false> dist;
            for (auto it = v.begin(), end = v.end(); end != it; ++it) {
                FiveBestPoints fiveBestA, fiveBestB;
                fiveBestA.fill(std::make_pair(
                            v.end(), std::numeric_limits<T>::max()));
                fiveBestB = fiveBestA;
                // find the five lowest distance points to *it the fast
                // way...
                find_n_nearest_kdtree<K>(v.begin(), end, *it,
                        fiveBestA.begin(), fiveBestA.end(), cmp, dist);
                // find the five lowest distance points the slow way...
                for (auto jt = v.begin(), jtend = v.end();
                        jtend != jt; ++jt) {
                    const auto d = dist(*it, *jt, std::size_t(-1));
                    // check if worse than best five so far
                    if (d > fiveBestB[4].second) continue;
                    // better - check where to insert
                    auto kt = std::lower_bound(
                        fiveBestB.begin(), fiveBestB.end(), d,
                        [] (const GoodPoint& a, T b) noexcept
                        { return a.second < b; });
                    // make space for new value
                    std::copy_backward(kt, fiveBestB.end() - 1, fiveBestB.end());
                    // and put new value into array of five best points
                    *kt = std::make_pair(jt, d);
                }
                // okay, compare both lists of points
                for (auto jt = fiveBestA.begin(), jtend = fiveBestA.end(),
                        kt = fiveBestB.begin(); jtend != jt; ++jt, ++kt) {
                    // we either need an exact match...
                    if (*jt == *kt) continue;
                    // ... or competing points at the same distance
                    if (jt->second == kt->second) continue;
                    // anything else is not fine...
                    okay = false;
                    break;
                }
                if (!okay) break;
            }
            if (!okay) ++nfail;
        }
    }
    report("Summary: %u/%u tests failed\n", nfail, ntot);
    return std::make_pair(nfail, ntot);
}

int main(int /* argc */, char* /* argv*/[])
{
    // list all tests to be run
    std::function<std::pair<unsigned, unsigned> ()> tests[] = {
        [] () { return test1D(); },
        [] () { return testKD<1>(); },
        [] () { return testKD<2>(); },
        [] () { return testKD<3>(); },
        [] () { return testKD<4>(); },
        [] () { return testKD<5>(); },
        [] () { return testNN1<1>(); },
        [] () { return testNN1<2>(); },
        [] () { return testNN1<3>(); },
        [] () { return testNN1<4>(); },
        [] () { return testNN1<5>(); },
        [] () { return testNN2<1>(); },
        [] () { return testNN2<2>(); },
        [] () { return testNN2<3>(); },
        [] () { return testNN2<4>(); },
        [] () { return testNN2<5>(); },
        [] () { return test5NN<1>(); },
        [] () { return test5NN<2>(); },
        [] () { return test5NN<3>(); },
        [] () { return test5NN<4>(); },
        [] () { return test5NN<5>(); },
    };
    // run all listed tests
    auto result = std::accumulate(std::begin(tests), std::end(tests),
            std::make_pair(0u, 0u), [] (std::pair<unsigned, unsigned> a,
                decltype(tests[0]) f) noexcept {
            auto b = f();
            return std::make_pair(a.first + b.first, a.second + b.second); });
    // and print the result
    std::printf("Summary: %u/%u tests failed\n",
            result.first, result.second);
    return (0 == result.first) ? 0 : 1;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
